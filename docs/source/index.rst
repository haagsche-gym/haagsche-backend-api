.. Haagsche Gym GraphQL Backend API documentation master file, created by
   sphinx-quickstart on Sat May 23 17:58:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Haagsche Gym GraphQL Backend API's documentation!
============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
