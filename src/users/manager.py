from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email: str, password: str, **extra_fields):
        '''Create and save a user with the given email and password

        password will be hashed before saveing.

        :param email: unique email address
        :type email: str
        :param password: unhashed user password
        :type password: str
        :return: saved user model instance, with email and hashed password
        :rtype: User
        '''
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, **extra_fields):
        '''create a none staff and none super user.

        :return: saved user model instance
        :rtype: User
        '''
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self._create_user(**extra_fields)

    def create_superuser(self, **extra_fields):
        '''Create a super user.

        :raises ValueError: if is_staff or is_superuser fields are set to false
        :return: saved user model instance
        :rtype: User
        '''
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)  # does not have to be True

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True.")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True.")

        return self._create_user(**extra_fields)
