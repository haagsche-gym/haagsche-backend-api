from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.fields import MonitorField
from model_utils.models import TimeStampedModel

from .manager import UserManager


class User(TimeStampedModel, AbstractBaseUser, PermissionsMixin):
    """Django Athentication User model.

    This model will replace the default internal Django authentication user model

    :param AbstractBaseUser: Django's build-in abstract base user model,
    holds the essential fields  to support django build-in authentication backend
    :param PermissionMixin: Django's build-in permission mixin's,
    holds the essential fields to support django's build-in Group and Permission models
    """

    email = models.EmailField(
        _("email address"),
        unique=True,
        error_messages={"unique": _("A user with that email already exists.")},
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=False,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    activity_changed = MonitorField(monitor='is_active')

    objects = UserManager()

    USERNAME_FIELD = "email"

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")
        ordering = ()

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)
