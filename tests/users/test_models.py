import pytest
from django.contrib.auth import get_user_model
from django.db.utils import IntegrityError

from .factories import UserFactory

User = get_user_model()
pytestmark = pytest.mark.django_db


def test_email_field_is_unique():
    user = UserFactory()

    assert User.objects.count() == 1
    with pytest.raises(IntegrityError):
        User.objects.create_user(email=user.email, password=user._password)


# TODO: test mail functionality


def test_create_user():
    """Check if fields are correct when a user is created"""
    user = UserFactory()

    # Default fields
    assert not user.is_staff
    assert not user.is_superuser
    assert not user.is_active
    # TimestampModel is inherted
    assert user.created
    assert user.modified


def test_is_active_monitor_field():
    """Check that activity_changed field is monitoring is_active field."""
    user = UserFactory()
    assert user.activity_changed
    assert not user.is_active
    previous_activity = user.activity_changed
    user.is_active = True
    user.save()
    assert user.is_active
    assert previous_activity != user.activity_changed


def test_create_superuser():
    """Check if fields are correct when superuser is created."""
    _user = UserFactory.build()

    user = User.objects.create_superuser(email=_user.email, password=_user._password)
    assert user.is_active
    assert user.is_superuser
    assert user.is_staff


def test_create_inactive_superuser():
    """Check if is_active field is correct when creating an inactive superuser."""
    _user = UserFactory.build()

    user = User.objects.create_superuser(
        email=_user.email, password=_user._password, is_active=False
    )
    assert not user.is_active


def test_explicit_non_staff_superuser():
    """Raise ValueError if is_staff field is False at superuser creation."""
    _user = UserFactory.build()

    with pytest.raises(ValueError):
        User.objects.create_superuser(
            email=_user.email, password=_user._password, is_staff=False
        )


def test_explicit_non_superuser_superuser():
    """Raise ValueError if is_superuser field is False at superuser creation."""
    _user = UserFactory.build()

    with pytest.raises(ValueError):
        User.objects.create_superuser(
            email=_user.email, password=_user._password, is_superuser=False
        )
