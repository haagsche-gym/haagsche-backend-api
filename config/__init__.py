"""Django configuration Package.

These configurations are use to setup the Django framework structure for the is project.

Includes the following:
    - Django Settings package
    - Django urls
    - Webserver configurations
"""
import typing as t

# django debug toolbar
DEBUG_TOOL: t.Tuple[str] = (
    "debug_toolbar",
    "graphiql_debug_toolbar",
)
# applications used to serve project documentation on django's server
SERVE_DOCS: t.Tuple[str] = (
    "sphinxdoc",
    "haystack",
)
