"""
This module contains the projects graphql schema.

with all registered Query and Mutation graphene.ObjectTypes.
"""
import graphene


class Query(graphene.ObjectType):
    """Registers all of the project query graphene.ObjectTypes."""

    pass


class Mutation(graphene.ObjectType):
    """Registers all of the project mutation graphene.ObjectTypes."""

    pass


schema: graphene.Schema = graphene.Schema()
