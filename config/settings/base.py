"""Base is the base settings configuration for django project.

All other settings configurations are build upon this one.
"""

import sys
import typing as t

from environ import Env, Path

ROOT_DIR: Path = (Path(__file__) - 3)  # **/{project-root}
BASE_DIR: str = str(ROOT_DIR)
APPS_DIR: str = ROOT_DIR.path("src")  # **/{project-root}/src
# This allows easy placement of apps within the project
sys.path.append(str(APPS_DIR))
env: Env = Env()


# GENERAL
# ------------------------------------------------------------------------------
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE: str = "UTC"
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE: str = "en-us"
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N: bool = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N: bool = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ: bool = True

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF: str = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION: str = "config.wsgi.application"

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS: t.List[str] = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]

THIRD_PARTY_APPS: t.List[str] = [
    "graphene_django",
]

LOCAL_APPS: t.List[str] = [
    # Your stuff: custom apps go here
    "users.apps.UsersConfig",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS: t.List[str] = [*DJANGO_APPS, *THIRD_PARTY_APPS, *LOCAL_APPS]

# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL: str = "users.User"

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES: t.Dict[str, t.Any] = {
    "default": env.db(
        "DATABASE_URL",
        "postgres://{user}:{password}@{host}:{port}/{db}".format(
            user=env.str("POSTGRES_USER"),
            password=env.str("POSTGRES_PASSWORD"),
            host=env.str("POSTGRES_HOST"),
            port=env.int("POSTGRES_PORT"),
            db=env.str("POSTGRES_DB"),
        ),
    )
}
DATABASES["default"]["ATOMIC_REQUESTS"]: bool = True


# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES: t.Dict[str, str] = {"sites": "contrib.sites.migrations"}

# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS: t.List[t.Dict[str, str]] = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE: t.List[str] = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL: str = "/static/"

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES: t.List[t.Dict[str, t.Any]] = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        "DIRS": [str(APPS_DIR.path("templates"))],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    }
]

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL: str = "admin/"
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS: t.List[t.Tuple[str]] = [("admin", "admin@admin.com")]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS: str = ADMINS

# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING: t.Dict[str, t.Any] = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}

# Graphene
# ------------------------------------------------------------------------------
GRAPHENE = {
    "SCHEMA": "config.graphql.schema",
}
