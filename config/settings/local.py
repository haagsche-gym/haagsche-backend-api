"""Project settings configuration for local development environment."""

from .. import DEBUG_TOOL, SERVE_DOCS
from .base import *  # noqa
from .base import env, t

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG: bool = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY: str = env(
    "DJANGO_SECRET_KEY",
    default="Z4CE9Wv432jLad3pzfBJ5mcyCGQeoctXgGIp7maFadmzlI9M7cgPjj3AuHcwQ5r1",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
ALLOWED_HOSTS: t.List[str] = ["localhost", "0.0.0.0", "127.0.0.1"]

# Your stuff...
# ------------------------------------------------------------------------------

INSTALLED_APPS += [  # noqa F405
    *DEBUG_TOOL,
    *SERVE_DOCS,
    "django_extensions",
]

# Sphinxdoc
# ------------------------------------------------------------------------------
# https://django-sphinxdoc.readthedocs.io/en/latest/quickstart.html
SPHINXDOC_BUILD_DIR: str = str(ROOT_DIR.path("docs").path("build"))

DOCS_URL = "docs/"

# django-debug-toolbar
# ------------------------------------------------------------------------------
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
MIDDLEWARE += [  # noqa F405
    "graphiql_debug_toolbar.middleware.DebugToolbarMiddleware",
]
# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG: t.Dict[str, t.Any] = {
    "DISABLE_PANELS": ["debug_toolbar.panels.redirects.RedirectsPanel", ],
    "SHOW_TEMPLATE_CONTEXT": True,
}
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS: t.List[str] = ["127.0.0.1", "10.0.2.2"]

# Haystack https://django-haystack.readthedocs.io/en/master/index.html
# ------------------------------------------------------------------------------
HAYSTACK_CONNECTIONS = {
    "default": {"ENGINE": "haystack.backends.simple_backend.SimpleEngine", },
}
