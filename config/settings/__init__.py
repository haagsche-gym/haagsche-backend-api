'''Django settings package.

Environment specific settings modules for Django framework setup.

Includes the following settings environments:
    - base
    - local
    - test
    - production
'''
