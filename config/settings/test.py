'''Django test settings configurations.'''

from .base import *  # noqa
from .base import env, t

# GENERAL
# ------------------------------------------------------------------------------
DEBUG = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY: str = env.str(
    "DJANGO_SECRET_KEY",
    default="4P03GIHPH0bqCW8QBmQtts4kzrxoes3TYtokl78ZMy30Tls1QYTwLOv1NczmlXZL",
)
# https://docs.djangoproject.com/en/dev/ref/settings/#test-runner
TEST_RUNNER: str = "django.test.runner.DiscoverRunner"

# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES: t.Dict[str, t.Any] = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS: t.List[str] = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES[-1]["OPTIONS"]["loaders"] = [  # type: ignore[index] # noqa F405
    (
        "django.template.loaders.cached.Loader",
        [
            "django.template.loaders.filesystem.Loader",
            "django.template.loaders.app_directories.Loader",
        ],
    )
]

# Your stuff...
# ------------------------------------------------------------------------------
