"""Django API url/routs configuration."""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView

urlpatterns = [
    path("graphql/", csrf_exempt(GraphQLView.as_view(graphiql=True)), name="graphql"),
]
if settings.DEBUG:
    urlpatterns += [
        path(settings.ADMIN_URL, admin.site.urls),
        path(settings.DOCS_URL, include("sphinxdoc.urls", namespace=None)),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns += [
            path("__debug__/", include(debug_toolbar.urls, namespace="debug-toolbar")),
        ]
